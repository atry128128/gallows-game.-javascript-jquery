let alphabetBoard = new Array(26);
let alphabet = "";
let passwordsArray = new Array(5);
passwordsArray = ['codding is amazing', 'javascript is the best', 'practice makes perfect', 'better late than never', 'knowledge is power'];
let random = Math.floor(Math.random() * 5);
let password = passwordsArray[random];
let hiddenPassword = "";
let passwordLength = password.length;
let missedCounter = 0;

const hit = new Audio("https://www.pacdv.com/sounds/voices/yes-2.wav");
const notHit = new Audio("https://www.pacdv.com/sounds/voices/no-1.wav");

window.onload = startApp;

hiddenPassword = Array.from(password).map((value) => value !== " " ? "-" : " ").join("");


function showPassword() {
    document.getElementById("password").innerHTML = hiddenPassword;
}

function startApp() {
    alphabetBoard = "abcdefghijklmnopqrstuvwxyz".toUpperCase().split("");
    let albagetLength = alphabetBoard.length;

    for (i = 0; i < albagetLength; i++) {
        if (i % 7 == 0) alphabet += `<br>`;
        alphabet += `<div class='letter' onclick='check(${i})' id='a${i}'>${alphabetBoard[i]}</div>`
    }
    document.getElementById("alphabet").innerHTML = alphabet;
    showPassword();
}


function getLetterStyle(didHit) {
    const didHitLetterStyle = {
        border: "3px solid green",
        cursor: "default",
        background: "rgb(0, 51, 0)",
        filter: "brightness(100%)",
        transform: "scale(1)"
    };

    const didNotHitLetterStyle = {
        border: "3px solid red",
        cursor: "default",
        background: "rgb(51, 0, 0)",
        filter: "brightness(100%)",
        transform: "scale(1)"
    };

    return didHit ? didHitLetterStyle : didNotHitLetterStyle;
}

function check(id) {

    let letter = document.getElementById(`a${id}`).textContent;
    let passwordUppercase = password.toUpperCase();
    let didItHit = false;


    for (i = 0; i < passwordLength; i++) {

        if (passwordUppercase.charAt(i) === letter) {

            //hit :)
            didItHit = true;

            let style = document.getElementById(`a${id}`).style;
            style = Object.assign(style, getLetterStyle(didItHit));

            hiddenPassword = hiddenPassword.revealLetter(i, letter);

        } else if (passwordUppercase.charAt(i) !== letter && !didItHit) {

            //not hit :(
            let style = document.getElementById(`a${id}`).style;
            style = Object.assign(style, getLetterStyle(didItHit));
        }
    }

    if (didItHit) hit.play();
    else notHit.play();

    if (passwordUppercase.charAt(i) !== letter && !didItHit) {
        missedCounter++;
        if (missedCounter === 9) {

            showLooseScreen();
        }
        document.getElementById("gallows").innerHTML = `<img src='img/s${missedCounter}.jpg'/>`;
    }

    if (password.toUpperCase() === hiddenPassword) {

        showWinScreen();
    }
    showPassword();
}

function changeIcon(){
    $('.icon').toggleClass('icon-change-color');
}

function showWinScreen() {
    document.getElementById("alphabet").innerHTML = `<div class='end-message'>You are winner!<br><br><i class='far fa-laugh-beam'></i><br><br><span class='repeat repeat-winner' onclick='location.reload()'>Do you want to play again?</span>`;
}

function showLooseScreen() {
    document.getElementById("alphabet").innerHTML = `<div class='end-message'>You are loser! <br><br><i class='far fa-frown'></i><br><br>The sollution is:<br> ${password}</div><span class='repeat' onclick='location.reload()'>Do you want to play again?</span>`;

    $('.icon').addClass('icon-loser');
    $('.icon').removeClass('icon-change-color');
}


String.prototype.revealLetter = function (where, what) {
    return this.substr(0, where) + what + this.substr(where + 1);
}

function showForm() {
    $('#knownPassword').css('display', 'none');
    $('#formToPassword').css('display', 'flex');

}

function checkPass() {

    let pass = document.formPass.pass.value;
    $('#formToPassword').css('display', 'none');

    if (pass.toLowerCase() === password) {

        $('#icon-spinner').css('display', 'flex');

        setTimeout(function () {
            showWinScreen();
            $('#icon-spinner').css('display', 'none');
        }, 5000);


    } else {

        $('#icon-spinner').css('display', 'flex');

        setTimeout(function () {
            $('#wrongPass').css('display', 'flex');
            $('#icon-spinner').css('display', 'none');
        }, 3000);

        setTimeout(function () {
            $('#knownPassword').css('display', 'flex');
            $('#wrongPass').css('display', 'none');
            document.getElementById('pass').value = '';
        }, 9000);
    }
}




